from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import permissions
from .models import Params, Statistic
import datetime


class ApiOverview(APIView):
    permission_classes = (permissions.AllowAny,)

    def get(self, request):
        api_urls = {
            'Регистрирация в системе поисковой фразы и региона, возвращение id пары.': '/api/add/?region=...&search=...',
            "Принимает на вход id связки поисковая фраза + регион и интервал, за который нужно вывести счётчики."
            'Возвращает счётчики и соответствующие им временные метки (timestamp).': '/api/stat/?params=...&s_year=..&s_month=...&s_day=...&f_year=...&f_month=...&f_day=...'
        }
        return Response(api_urls)


class Add(APIView):
    """
        Регистрирация в системе поисковой фразы и региона, возвращение id этой пары.\n
        Параметры: region, search\n
        Например, http://localhost:8000/api/add/?search=samsung&region=moskva
    """
    permission_classes = (permissions.AllowAny,)

    def get(self, request):
        search = self.request.query_params.get('search')
        region = self.request.query_params.get('region')
        records = Params.objects.filter(search=search, region=region)
        if len(records) == 0:
            new_record = Params(search=search, region=region)
            new_record.save()
            return Response(new_record.id)
        else:
            return Response(records[0].id)


class Stat(APIView):
    """
        Принимает на вход id связки поисковая фраза + регион и интервал, за который нужно вывести счётчики.\n
        Параметры:\n
        id пары - params\n
        Старт временного интервала: s_year, s_month, s_day\n
        Конец временного интервала: f_year, f_month, f_day\n
        Например, http://localhost:8000/api/stat/?params=23&s_year=2020&s_month=1&s_day=1&f_year=2021&f_month=1&f_day=1
    """
    permission_classes = (permissions.AllowAny,)

    def get(self, request):
        params = self.request.query_params.get('params')
        s_year = int(self.request.query_params.get('s_year'))
        s_month = int(self.request.query_params.get('s_month'))
        s_day = int(self.request.query_params.get('s_day'))
        f_year = int(self.request.query_params.get('f_year'))
        f_month = int(self.request.query_params.get('f_month'))
        f_day = int(self.request.query_params.get('f_day'))
        records = Statistic.objects.filter(params=params, time__gte=datetime.date(s_year, s_month, s_day),
                                           time__lte=datetime.date(f_year, f_month, f_day))
        data = {}
        for i in records:
            data[str(i.time.date()) + ' ' + str(i.time.time())] = i.count
        return Response(data)
