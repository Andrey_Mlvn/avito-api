from django.db import models


class Params(models.Model):
    search = models.CharField(max_length=150)
    region = models.CharField(max_length=50)


class Statistic(models.Model):
    params = models.ForeignKey(Params, on_delete=models.CASCADE, related_name='params')
    time = models.DateTimeField(auto_now_add=True)
    count = models.IntegerField()
