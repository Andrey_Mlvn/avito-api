from django.urls import path, re_path
from api import views
from rest_framework_swagger.views import get_swagger_view

schema_view = get_swagger_view(title='Pastebin API')

urlpatterns = [
    path("", views.ApiOverview.as_view()),
    re_path(r'^add/.*$', views.Add.as_view(), name='something'),
    re_path(r'^stat/.*$', views.Stat.as_view(), name='something'),
]
