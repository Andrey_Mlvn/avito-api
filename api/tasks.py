import requests
from bs4 import BeautifulSoup
import re
from .models import Params, Statistic
from celery.task import periodic_task
from datetime import timedelta


@periodic_task(run_every=(timedelta(hours=1)), name='add1')
def add1():
    all_pairs = Params.objects.all()
    for pair in all_pairs:
        try:
            r = requests.get('https://www.avito.ru/' + pair.region + '?q=' + pair.search)
            soup = BeautifulSoup(r.text, 'lxml')
            res = soup.findAll("span", {"class": "page-title-count-1oJOc"})
            count = re.findall(r'>(.+?)<\/span>', str(res))[0]
            record = Statistic(params=pair, count=int(count.replace(' ', '')))
            record.save()
        except Exception as e:
            pass
